FROM maven:3.6.3-jdk-14 AS build
ARG app_name=financelog
RUN echo "Building app with ${app_name}"
COPY target/${app_name}*.jar /app.jar
CMD java $JAVA_OPTS -jar /app.jar
EXPOSE 8080
