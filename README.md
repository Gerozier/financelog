# FinanceLog

This project is part of the overall FinanceLog project. 

Check out the other repositories here:
- Frontend: https://gitlab.com/Gerozier/financelog_fe
- Security: https://gitlab.com/Gerozier/financelog_security

## Contributor

- Brämer, Dennis https://gitlab.com/Gerozier
- Schmidt-Berschet, Lisa https://gitlab.com/LisaSila
- Szalay, Gergely https://gitlab.com/szalayg

## Explanation 

We want to create a simple housekeeping book web app to improve our skills during this pandemic. 

## Approach 

At the beginning of the project we set a bunch of milestones. Every sprint we assign the different tasks 
to each contributor. 
In the stage we did pair programming, but when ever it makes sense we program on our own. 

### Personas

Gisela: 
- female, 36 years old
- single mom
- nurse

Expectations:
- overview of the income and expenses
- create a savings plan
- assign specific expenses to the child

###URL's
Swagger - http://localhost:8080/swagger-ui.html
