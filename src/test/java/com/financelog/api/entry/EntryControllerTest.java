package com.financelog.api.entry;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class EntryControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private EntryRepository entryRepository;

    private TestRestTemplate testRestTemplate = new TestRestTemplate();

    private String createUrlWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    private void setup() {
        entryRepository.deleteAll();

        entryRepository.save(createEntry("Essen", new BigDecimal("100.00"), true));
        entryRepository.save(createEntry("Kaffee", new BigDecimal("10.00"), false));
        entryRepository.save(createEntry("Kino", new BigDecimal("25.00"), false));
    }

    private static Entry createEntry(String title, BigDecimal value, boolean fixCost) {
        Entry entry = Entry.builder()
                .id(UUID.randomUUID().toString())
                .clientId(UUID.randomUUID().toString())
                .title(title)
                .value(value)
                .fixCost(fixCost)
                .category(null)
                .causer(null)
                .build();
        return entry;
    }


    @Test
    void getAllEntries() {
        setup();
        List<Entry> allEntries = testRestTemplate.getForObject(createUrlWithPort("/entries"), List.class);
        assertThat(allEntries).hasSize(3);
    }

    @Test
    void getAllEntries_no_data() {
        List<Entry> allEntries = testRestTemplate.getForObject(createUrlWithPort("/entries"), List.class);
        assertThat(allEntries).isEmpty();
    }

    @Test
    void getOneEntry() {
        setup();
        String clientId = entryRepository.findAll().iterator().next().getClientId();
        Entry entry = testRestTemplate.getForObject(createUrlWithPort("/entry/" + clientId), Entry.class);
        assertThat(entry.getTitle()).isEqualTo("Essen");
    }

    @Test
    void getOneEntry_no_valid_id() {
        setup();
        String clientId = "no or wrong id";
        Entry entry = testRestTemplate.getForObject(createUrlWithPort("/entry/" + clientId), Entry.class);
        assertThat(entry).isNull();
    }

    @Test
    void createEntry() {
        setup();
        EntryDTO newEntryDTO = new EntryDTO("Computer", false, new BigDecimal("1000"));
        testRestTemplate.postForObject(createUrlWithPort("/entries"), newEntryDTO, EntryDTO.class);
        List<Entry> allEntries = testRestTemplate.getForObject(createUrlWithPort("/entries"), List.class);
        assertThat(allEntries).hasSize(4);
    }

    @Test
    void updateEntry() {
        setup();
        EntryDTO updatetedEntry = new EntryDTO("Essen Penny", true, new BigDecimal("95"));
        String clientId = entryRepository.findAll().iterator().next().getClientId();
        Entry entry = testRestTemplate.getForObject(createUrlWithPort("/entry/" + clientId), Entry.class);
        entry.setTitle(updatetedEntry.getTitle());
        testRestTemplate.put(createUrlWithPort("/entry/" + clientId), entry, Entry.class);
        assertThat(entry.getTitle()).isEqualTo("Essen Penny");
    }

    @Test
    void deleteEntry() {
        setup();
        String clientId = entryRepository.findAll().iterator().next().getClientId();
        testRestTemplate.delete(createUrlWithPort("/entry/" + clientId));
        assertThat(entryRepository.findEntryByClientId(clientId)).isNull();
    }

    @Test
    void deleteEntry_no_valid_id() {
        setup();
        String clientId = "no or wrong id";
        testRestTemplate.delete(createUrlWithPort("/entry/" + clientId));
        assertThat(entryRepository.findEntryByClientId(clientId)).isNull();
    }
}
