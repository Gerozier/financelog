package com.financelog.api.entry;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.financelog.api.category.Category;
import com.financelog.api.causer.Causer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@Entity
@Builder
public class Entry {

    @Id
    @JsonIgnore
    private String id;

    private String clientId;

    private String title;
    private boolean fixCost;
    private BigDecimal value;

    @ManyToOne
    private Causer causer;

    @ManyToOne
    private Category category;

    public Entry() {
    }


}
