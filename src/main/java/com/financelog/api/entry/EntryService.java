package com.financelog.api.entry;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class EntryService {

    private EntryRepository entryRepository;

    public EntryService(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    public ResponseEntity<Object> getAllEntries() {
        List<Entry> entryRepositoryAll = (List<Entry>) entryRepository.findAll();
        if (entryRepositoryAll.isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(Collections.emptyList());
        }
        return ResponseEntity.ok(entryRepositoryAll);
    }

    public ResponseEntity<Object> getOneEntryByClientId(String clientId) {
        Entry entryByClientId = entryRepository.findEntryByClientId(clientId);
        if (entryByClientId == null) {
            return ResponseEntity
                    .notFound()
                    .build();
        }
        return ResponseEntity.ok(entryByClientId);
    }

    public ResponseEntity<Object> createEntry(EntryDTO entryDTO) {

        Entry newEntry = new Entry(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                entryDTO.getTitle(),
                entryDTO.isFixCost(),
                entryDTO.getValue(),
                null,
                null);

        entryRepository.save(newEntry);

        return ResponseEntity.ok(newEntry);
    }

    public ResponseEntity<Object> deleteEntryByClientId(String clientId) {

        Entry entryByClientId = entryRepository.findEntryByClientId(clientId);
        if (entryByClientId == null) {
            return ResponseEntity
                    .notFound()
                    .build();
        }
        entryRepository.delete(entryByClientId);

        return ResponseEntity.ok().build();
    }

    public ResponseEntity<Object> updateEntryByClintId(EntryDTO entryDTO, String clientId) {

        Entry entryByClientId = entryRepository.findEntryByClientId(clientId);
        entryByClientId.setTitle(entryDTO.getTitle());
        entryByClientId.setFixCost(entryDTO.isFixCost());
        entryByClientId.setValue(entryDTO.getValue());
        entryRepository.save(entryByClientId);

        return ResponseEntity.ok(entryByClientId);
    }


}
