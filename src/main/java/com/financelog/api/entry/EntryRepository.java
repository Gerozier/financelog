package com.financelog.api.entry;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EntryRepository extends CrudRepository <Entry, String> {
    Entry findEntryByClientId(String clientID);
}

