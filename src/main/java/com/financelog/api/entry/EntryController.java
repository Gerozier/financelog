package com.financelog.api.entry;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class EntryController {

    private EntryService entryService;

    public EntryController(EntryService entryService) {
        this.entryService = entryService;
    }

    @GetMapping("/entries")
    public ResponseEntity<Object> getAllEntries() {
        return entryService.getAllEntries();
    }

    @GetMapping("/entry/{id}")
    public ResponseEntity<Object> getOneEntry(@PathVariable String id) {
        return entryService.getOneEntryByClientId(id);
    }

    @PostMapping("/entries")
    public ResponseEntity<Object> createEntry(@RequestBody EntryDTO entryDTO) {
        return entryService.createEntry(entryDTO);
    }

    @PutMapping("/entry/{id}")
    public ResponseEntity<Object> updateEntry(@RequestBody EntryDTO entryDTO, @PathVariable String id) {
        return entryService.updateEntryByClintId(entryDTO, id);
    }

    @DeleteMapping("/entry/{id}")
    public ResponseEntity<Object> deleteEntry(@PathVariable String id) {
        return entryService.deleteEntryByClientId(id);
    }

}
