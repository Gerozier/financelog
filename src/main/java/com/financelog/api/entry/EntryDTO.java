package com.financelog.api.entry;

import java.math.BigDecimal;

public class EntryDTO {

    private String title;
    private boolean fixCost;
    private BigDecimal value;
    private String clientId;

    public EntryDTO() {
    }

    public EntryDTO(String title, boolean fixCost, BigDecimal value) {
        this.title = title;
        this.fixCost = fixCost;
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isFixCost() {
        return fixCost;
    }

    public void setFixCost(boolean fixCost) {
        this.fixCost = fixCost;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;


    }
}
