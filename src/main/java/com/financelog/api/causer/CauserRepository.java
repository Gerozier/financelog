package com.financelog.api.causer;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CauserRepository extends CrudRepository<Causer, String> {
}
