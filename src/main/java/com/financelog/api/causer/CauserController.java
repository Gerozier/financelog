package com.financelog.api.causer;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
public class CauserController {

	private CauserRepository causerRepository;

	public CauserController(CauserRepository causerRepository) {
		this.causerRepository = causerRepository;
	}

	@GetMapping("/getCausers")
	public List<Causer> getAllCauser() {
		List<Causer> all = (List<Causer>) causerRepository.findAll();
		return all;
	}

	@GetMapping("/getCauser/{id}")
	public Optional<Causer> getCauser(@PathVariable String id) {
		return causerRepository.findById(id);
	}

	@PostMapping("/createCauser")
	public void getAllCauser(@RequestBody Causer causer) {
		causerRepository.save(causer);
	}


}
