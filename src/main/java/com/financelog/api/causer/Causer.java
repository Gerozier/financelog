package com.financelog.api.causer;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Data
@AllArgsConstructor
@Entity
public class Causer {

    @Id
    private String id = UUID.randomUUID().toString();;
    private String name;


    public Causer() {
    }
}
