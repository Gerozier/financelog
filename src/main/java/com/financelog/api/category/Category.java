package com.financelog.api.category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Data
@AllArgsConstructor
@Entity
public class Category {

    @Id
    @JsonIgnore
    private String id = UUID.randomUUID().toString();

    private String clientId = UUID.randomUUID().toString();
    private String catName;

    public Category() {
    }
}
