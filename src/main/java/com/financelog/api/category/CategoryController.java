package com.financelog.api.category;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class CategoryController  {

    private CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("/categories")
    public List<Category> getAllCaterorys() {
        List<Category> all = (List<Category>) categoryRepository.findAll();
        return all;
    }

    @PostMapping("/categories")
    public void createCategory(@RequestBody Category category) {
        categoryRepository.save(category);
    }

}
